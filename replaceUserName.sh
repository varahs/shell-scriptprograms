#!/bin/bash -x

#User Input and Replace String Template “Hello <<UserName>>, How are you?” 
#I/P -> Take User Name as Input. Ensure UserName has min 3 char
#Logic -> Replace <<UserName>> with the proper name
#O/P -> Print the String with User Name 
# Author : varsha s
# DAte : 30 Dec 2019

echo Enter User Name ;
read userName;
while [ ${#userName} -lt 3 ]
do
	echo ;
	echo Username should be minimum 3 character;
	echo Re-enter username;
	read userName;
done

#if [  ${#userName} == 3 -o ${#userName} -gt 3 ]
#then
	echo Hello $userName, How are you?
#fi


